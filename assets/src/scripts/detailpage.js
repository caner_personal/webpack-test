import Module1 from "./modules/module-1";
class DetailPage {
    constructor() {
        console.log("Detay sayfa");
        this.init();
    }
    init(){

        let name = "webpack-test-head";
        document.querySelector("h1").classList.add(name);

        const ul = document.querySelector('.list');
        const array = ['webpack', 'test', 'project', 'for', 'start'];

        array.forEach(item => {
            console.log(item);
            ul.innerHTML += `<li>${item}</li>`;
        });

        Promise.resolve(1);
        console.error("Test for detailpage.js sourcemap");

        new Module1;
    }
}

window.addEventListener('DOMContentLoaded', (event) => {
    new DetailPage;
});


