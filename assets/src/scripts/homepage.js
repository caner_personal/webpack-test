import Module1 from "./modules/module-1";
class Homepage {
    constructor() {
        console.log("anasayfa");
        this.init();
    }
    init(){

        let name = "webpack-test-head";
        document.querySelector("h1").classList.add(name);

        const ul = document.querySelector('.list');
        const array = ['webpack', 'test', 'project', 'for', 'start'];

        array.forEach(item => {
            console.log(item);
            ul.innerHTML += `<li>${item}</li>`;
        });

        Promise.resolve(1);
        console.error("Test for homepage.js sourcemap");

        new Module1;
    }
}
window.addEventListener('DOMContentLoaded', (event) => {
    new Homepage;
});


